# What's this all about?
# See https://snyk.io/blog/10-best-practices-to-containerize-nodejs-web-applications-with-docker/
#
# Using TypeScript? You must build the JS files first with `npm run build` on the on the same 
# machine as you run `docker build .`!

# --------------> The build image
FROM node:14 AS build
WORKDIR /usr/src/app
COPY package*.json /usr/src/app/
RUN --mount=type=secret,mode=0644,id=npmrc,target=/usr/src/app/.npmrc npm ci --only=production
 
# --------------> The production image
FROM node:14-alpine
RUN apk add dumb-init --no-cache
ENV NODE_ENV production
USER node
WORKDIR /usr/src/app
COPY --chown=node:node --from=build /usr/src/app/node_modules /usr/src/app/node_modules
COPY --chown=node:node . /usr/src/app
CMD ["dumb-init", "node", "dist/index.js"]
